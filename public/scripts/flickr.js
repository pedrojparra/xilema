'use strict';

var FlickrLibrary = ( function(){

  // Globals
  var XclaveFlickr = '';
  var XuserFlickr = '';
  var XgaleriasFlickr = {
    'DormitoriosGalery':'',
    'SalonesGalery':'',
    'AuxiliaresGalery':'',
    'PromocionesGalery':'',
    'AcabadosGalery':''
  };

  // Cache DOM
  var $el = $('#FlickrLibrary');
  var $cargando = $el.find('#FlickrLoading');
  var $itemContainer = $el.find('#FlickrItemsContainer');
  var $linkDormitorios = $('#linkDormitorios');
  var $linkSalones = $('#linkSalones');
  var $linkAuxiliares = $('#linkAuxiliares');
  var $linkPromociones = $('#linkPromociones');
  var $linkAcabados = $('#linkAcabados');

  $linkDormitorios.addClass('active');

  // Events
  $linkDormitorios.on('click', function(){
    resetActiveButton();
    $linkDormitorios.addClass('active');
    RemoveItemsContainer();
    Loading('show', function(){
      privateGetGalery( XgaleriasFlickr.DormitoriosGalery );
    });
  });

  $linkSalones.on('click', function(){
    resetActiveButton();
    $linkSalones.addClass('active');
    RemoveItemsContainer();
    Loading('show', function(){
      privateGetGalery( XgaleriasFlickr.SalonesGalery );
    });
  });

  $linkAuxiliares.on('click', function(){
    resetActiveButton();
    $linkAuxiliares.addClass('active');
    RemoveItemsContainer();
    Loading('show', function(){
      privateGetGalery( XgaleriasFlickr.AuxiliaresGalery );
    });
  });

  $linkPromociones.on('click', function(){
    resetActiveButton();
    $linkPromociones.addClass('active');
    RemoveItemsContainer();
    Loading('show', function(){
      privateGetGalery( XgaleriasFlickr.PromocionesGalery );
    });
  });

  $linkAcabados.on('click', function(){
    resetActiveButton();
    $linkAcabados.addClass('active');
    RemoveItemsContainer();
    Loading('show', function(){
      privateGetGalery( XgaleriasFlickr.AcabadosGalery );
    });
  });


  // DOM Utils

  function Loading(state, done) {
    switch (state) {
      case 'hidden':
        $cargando.fadeOut(300, function(){
          done();
        });
        break;
      default:
        $itemContainer.fadeOut(300, function(){
          $cargando.fadeIn(300, function(){ done(); });
        });
    }
  }

  function resetActiveButton() {
    $linkDormitorios.removeClass('active');
    $linkSalones.removeClass('active');
    $linkAuxiliares.removeClass('active');
    $linkPromociones.removeClass('active');
    $linkAcabados.removeClass('active');
  }

  function RemoveItemsContainer() {
    $itemContainer.find('.flickrItem').remove();
  }

  function getDescription(idPhoto, done) {
    var queryInfoFoto = "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key="+XclaveFlickr+"&photo_id="+idPhoto+"&format=json&nojsoncallback=1";
    $.getJSON( queryInfoFoto ).done(function( data ){
      return done( data.photo.description._content );
    });
  }

  // getCollections
  function privateGetGalery(idGalery) {
    var xIdGalery = ( typeof idGalery === 'string' ) ? idGalery : XgaleriasFlickr.DormitoriosGalery;

    var queryFotos = "https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key="+XclaveFlickr+"&photoset_id="+xIdGalery+"&format=json&nojsoncallback=1";
    // console.log(queryFotos);
    $.getJSON( queryFotos ).done(function( data ){

      RemoveItemsContainer();

      $.each(data.photoset.photo , function(index, item){

        var fotoGaleriaGrn = "http://farm"+item.farm+".staticflickr.com/"+item.server+"/"+item.id+"_"+item.secret+"_c.jpg";

        var template = "<div class='flickrItem hidden-xs hidden-sm col-md-4 col-lg-3'>";
        template    += "<div class='producto' id='"+item.id+"'>";
        template    += "<div class='info'><h5>"+item.title+"</h5><div class='linksBox'>";
        template    += "<a href='"+fotoGaleriaGrn+"' class='flickrGalery' rel='flickrGal' title='"+item.title+"' ><div class='linksBox-ampliar'>Ampliar</div></a>";
        template    += "<a href='/producto/"+item.id+"'><div class='linksBox-detalles'>Detalles</div></a>";
        template    += "</div></div>";
        template    += "<div class='foto' dataUrl='"+fotoGaleriaGrn+"')'></div><div class='loading'></div>";
        template    += "</div>";
        template    += "</div>";

        // Template mobile
        template    += "<div class='flickrItem hidden-md hidden-lg col-xs-12 col-sm-6'><a href='/producto/"+item.id+"'>";
        template    += "<div class='producto' id='"+item.id+"'>";
        template    += "<div class='foto' dataUrl='"+fotoGaleriaGrn+"')'></div><div class='loading'></div>";
        template    += "</div>";
        template    += "<div class='producto-footer'><h3>"+item.title+"</h3></div>";
        template    += "</a></div>";

        $itemContainer.append(template);

      });

      Loading('hidden', function(){

        $itemContainer.fadeIn(300);
        $('.producto').each(function(index, item){

          var $este = $(this);
          var $foto = $este.find('.foto');
          var imgUrl = $foto.attr('dataUrl');
          var idFoto = $este.attr('id');

          getDescription(idFoto, function(description){
            var fancyboxDesciption = $este.find('h5').text() +' - '+ description;
            // $este.find('h6').html(description);

            $este.parent('.flickrGalery').attr({title: fancyboxDesciption});
          });

          $('<img>').attr('src',function(){ return imgUrl; }).load(function(){
            $foto.css('background-image', 'url(' + imgUrl + ')');
            $este.find('.loading').fadeOut();
          });

          $('.flickrGalery').fancybox({
            openEffect : 'elastic',
            helpers: {
              title : {
                type: 'inside'
              },
              overlay: {
                locked: false
              }
            }
          });
        });
      });
    });
  }

  // setKeys
  function publicSetKeys(claveFlickr, userFlickr) {
    XclaveFlickr = (typeof claveFlickr === 'string') ? claveFlickr : '';
    XuserFlickr = (typeof userFlickr === 'string') ? userFlickr : '';
  }

  // setKeys
  function publicShowInfo() {
    console.log('Clave: ' + XclaveFlickr);
    console.log('User: ' + XuserFlickr);
    console.log('ID Galeria Dormitorios: ' + XgaleriasFlickr.DormitoriosGalery);
    console.log('ID Galeria Salones: ' + XgaleriasFlickr.SalonesGalery);
    console.log('ID Galeria Auxiliares: ' + XgaleriasFlickr.AuxiliaresGalery);
  }

  // setGalerys
  function publicSetGalerys(galeriasFlickr) {
    XgaleriasFlickr = galeriasFlickr;
  }

  // init
  function publicInit() {
    privateGetGalery();
  }

  // Export methods
  return {
    setKeys: publicSetKeys,
    showInfo: publicShowInfo,
    setGalerys: publicSetGalerys,
    init: publicInit
  };

})();

// Use
var claveFlickr = '75681368b9b367e7be5e98e844997fc2';
var userFlickr = '134315554@N03';
var galeriasFlickr = {
  'DormitoriosGalery': '72157655747677764',
  'SalonesGalery': '72157658058198342',
  'AuxiliaresGalery': '72157658084536851',
  'PromocionesGalery':'72157658970002866',
  'AcabadosGalery':'72157658538291486'
};

FlickrLibrary.setKeys(claveFlickr, userFlickr);
FlickrLibrary.setGalerys(galeriasFlickr);
// FlickrLibrary.showInfo();
FlickrLibrary.init();

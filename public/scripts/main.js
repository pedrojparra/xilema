/* globals google, $, Modernizr, window, console, document  */
'use strict';

// loader
function loader(){
  $('.Preloader-logo').delay(2000).fadeOut(800);
  $('.Preloader').delay(2000).fadeOut(800);
}

// Initapp

function initApp(){

/** Movil
*******************************************/
var isMovile = false;
if (Modernizr.touch){ isMovile = true; }

/** jQuery to collapse the navbar on scroll
*******************************************/
$(window).scroll(function() {
  if ($('.navbar').offset().top > 50) {
    $('.withoutMenu').find('.navbar-defined').fadeIn();
  } else {
    $('.withoutMenu').find('.navbar-defined').fadeOut();
  }
});

/** Fancybox
*******************************************/
 $('.fancybox').fancybox({
   openEffect : 'elastic',
   helpers: {
     overlay: {
       locked: false
     },
     thumbs: {
       width: 50,
       height: 50
     }
   }
 });

 /** WOW jS
 *******************************************/
new WOW({
  boxClass:     'wow',
  animateClass: 'animated',
  offset:       200,
  mobile:       false,
  live:         true
}).init();

/** Formulario
*******************************************/
$('#contactAlert').fadeOut('fast');
$('#contactSuccess').fadeOut('fast');

$('#micorreo').submit( function(event){

  event.preventDefault();
  $.ajax({

    url: '/ajaxForm',
    dataType: 'json',
    type: this.method,
    data: $(this).serialize(),
    success:function(data){

      console.log(data);
      if( data.status === 'enviado'){ $('#contactSuccess').fadeIn('fast'); }
      else{
        $('#contactAlert').fadeIn('fast');
      }

    },
    error:function(){
      $('#contactAlert').fadeIn('fast');
    }
  });

  return false;

});


/** Google map
*******************************************/

var hayMapa = document.getElementById('mapa');

function mapa(){
  var map, beachMarker, xLat = '37.3591792', xLon = '-4.4870686';
  var mapOptions = {
    zoom: 15,
    center: new google.maps.LatLng(xLat, xLon),
    scrollwheel: false,
    draggable: false,
  };

  var mapElement = document.getElementById('mapa');
  map = new google.maps.Map(mapElement, mapOptions);

  var image = './images/map-marker.png';
  var myLatLng = new google.maps.LatLng(xLat, xLon);
  beachMarker = new google.maps.Marker({
    position: myLatLng,
    map: map,
    icon: image
  });

  // google.maps.event.addListener(beachMarker, 'click', function() {
  // window.open('https://www.google.es/maps/place/Sillas+de+Lucena/@37.40694,-4.4719,15z/data=!4m2!3m1!1s0x0:0xf561657d0589bcd4','_blank');
  // });

}

if( hayMapa ){
  google.maps.event.addDomListener(window, 'load', mapa);
}


} // End initApp

$(document).ready(function() {

  loader();
  initApp();

});

'use strict';
var FlickrLibrary = (function(){
  // Globals
  var XclaveFlickr = '';
  var XuserFlickr = '';

  // Cache DOM
  var $el = $('.Producto-detail');
  var $idProducto = $el.find('#flickrId').text();
  var $foto = $el.find('.Producto-detail-foto img');
  var $info = $el.find('.Producto-detail-info');
  var $social = $el.find('.Producto-detail-social');
  var $head = $('head');

  function preparaMetaTwitter(name, content){
    return '<meta name="'+name+'" content="'+content+'"></>';
  }
  function preparaMetaFacebook(property, content){
    return '<meta property="'+property+'" content="'+content+'"></>';
  }

  function runeffects(){
    $foto.animate({opacity: 1}, 200);
    $info.animate({opacity: 1}, 200);
    $social.animate({opacity: 1}, 200);
  }

  // getDescription
  function getDescription(idPhoto, done) {
    var queryInfoFoto = "https://api.flickr.com/services/rest/?method=flickr.photos.getInfo&api_key="+XclaveFlickr+"&photo_id="+idPhoto+"&format=json&nojsoncallback=1";
    $.getJSON( queryInfoFoto ).done(function(data){

      var item = data.photo;

      // Foto producto
      var fotoProducto = 'http://farm'+item.farm+'.staticflickr.com/'+item.server+'/'+item.id+'_'+item.secret+'_c.jpg';
      $foto.attr('src', fotoProducto);

      // Titulo producto
      $info.find('h4').text(data.photo.title._content);

      // Description Producto
      var dataDescription = data.photo.description._content;
      // console.log(data.photo.description._content);
      var decoded = dataDescription.replace(/&quot;/g, '');
      var decodedForPage = decoded.replace(/\n/g,'<br />').replace(/\r/g,'<br />').replace(/\r\n/g,'<br />');
      $info.find('p').html(decodedForPage);

      // Social headers Facebook
      var appendToHead = preparaMetaFacebook('og:title', 'Xilema Mobiliario '+data.photo.title._content);
      appendToHead    += preparaMetaFacebook('og:type', 'article');
      appendToHead    += preparaMetaFacebook('og:image', fotoProducto);
      appendToHead    += preparaMetaFacebook('og:url', window.location.href);
      appendToHead    += preparaMetaFacebook('og:description', decoded);
      $head.append(appendToHead);
      appendToHead = '';

      // Social header Twitter
      appendToHead    += preparaMetaTwitter('twitter:card', 'summary');
      appendToHead    += preparaMetaTwitter('twitter:title', 'Xilema Mobiliario '+data.photo.title._content);
      appendToHead    += preparaMetaTwitter('twitter:description', decoded);
      appendToHead    += preparaMetaTwitter('twitter:image', fotoProducto);
      $head.append(appendToHead);
      appendToHead = '';




      // Twitter Social button
      var twitterLink = 'https://twitter.com/intent/tweet?text=Xilema Mobiliario.%20'+window.location.href;
      $social.find('.twitter_btn').attr('href', twitterLink);

      // Facebook Social button
      var facbookLink = 'https://www.facebook.com/sharer/sharer.php?u='+window.location.href;
      $social.find('.facebook_btn').attr('href', facbookLink);

      done();
    });
  }

  // Main
  function main(){

    $foto.animate({opacity: 0}, 100);
    $info.animate({opacity: 0}, 100);
    $social.animate({opacity: 0}, 100);

    getDescription($idProducto, function(){
      runeffects();
    });

  }

  // setKeys
  function publicSetKeys(claveFlickr, userFlickr) {
    XclaveFlickr = (typeof claveFlickr === 'string') ? claveFlickr : '';
    XuserFlickr = (typeof userFlickr === 'string') ? userFlickr : '';
  }

  // init
  function publicInit() {
    main();
  }

  // Export methods
  return {
    setKeys: publicSetKeys,
    init: publicInit
  };

})();



// Use
var claveFlickr = '75681368b9b367e7be5e98e844997fc2';
var userFlickr = '134315554@N03';
FlickrLibrary.setKeys(claveFlickr, userFlickr);
FlickrLibrary.init();

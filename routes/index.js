var express = require('express');
var router = express.Router();

// controllers
var homeController = require('../controllers/homeController');
var productosController = require('../controllers/productosController');
var catalogosController = require('../controllers/catalogosController');
var contactoController = require('../controllers/contactoController');
var sitemapController = require('../controllers/sitemapController');

router.param('productoId', productosController.load);

router.get('/', homeController.index);
router.get('/productos', productosController.index);
router.get('/producto/:productoId(\\d+)', productosController.show);
router.get('/catalogos', catalogosController.index);
router.get('/contacto', contactoController.index);
router.post('/ajaxForm', contactoController.ajaxForm);

// Sitemap
router.get('/sitemap.xml', sitemapController.index);

module.exports = router;

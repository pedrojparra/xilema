'use strict';

// Loading dependencies
var http = require('http');
var express = require('express');
var path = require('path');

// Initializing express application
var app = express();

// Deploy Openshift
var cc = require('config-multipaas');
var deploy = cc().add({IP: '127.0.0.1'});

// Body parser
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

// Logger
var logger = require('morgan');
app.use(logger('dev'));

// Cookies / Session
var cookieParser = require('cookie-parser');
app.use(cookieParser());

// Favicon
// var favicon = require('serve-favicon');
// app.use(favicon(__dirname + '/public/favicon.ico'));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(express.static(path.join(__dirname, 'public')));

// Disabling x-powered-by
app.disable('x-powered-by');

// Routes
var routes = require('./routes/index');
app.use('/', routes);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers
// development error handler
// will print stacktrace

if (app.get('env') === 'development') {
  app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
      title: 'error'
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
    title: 'error'
  });
});

app.listen(deploy.get('PORT'), deploy.get('IP'), function () {
  console.log('Listening on ' + deploy.get('IP') + ', port ' + deploy.get('PORT'));
});

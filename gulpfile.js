/* jshint esnext: true, strict: true */
'use strict';

var gulp = require('gulp');
var gulpLoadPlugins = require('gulp-load-plugins');
var browserSync = require('browser-sync');
var mainBowerFiles = require('main-bower-files');
var del = require('del');

var $ = gulpLoadPlugins();
var reload = browserSync.reload;

// Clean
gulp.task('clean', del.bind(null, [
  '.tmp',
  'public/fonts'
]));

// Styles
gulp.task('styles', function() {
  return gulp.src('public/sass/*.sass')
    .pipe($.plumber())
    .pipe($.sourcemaps.init())
    .pipe($.sass.sync({
      outputStyle: 'expanded',
      precision: 10,
      includePaths: ['.']
    }).on('error', $.sass.logError))
    .pipe($.autoprefixer({browsers: ['last 1 version']}))
    .pipe($.sourcemaps.write())
    .pipe(gulp.dest('public/css'))
    .pipe(reload({stream: true}));
});


// Fonts
gulp.task('fonts', function() {
  return gulp.src( mainBowerFiles({filter: '**/*.{eot,svg,ttf,woff,woff2}'}) )
    .pipe(gulp.dest('.tmp/fonts'))
    .pipe(gulp.dest('public/fonts'));
});

// Nodemon
gulp.task('nodemon', function(cb) {

  let called = false;

  $.nodemon({
    script: 'server.js',
    ext: 'js jade',
  })
  .on('start', function onStart() {
    if (!called) { cb(); }
    called = true;
  })
  .on('restart', function () {
    setTimeout( function () {
      reload({ stream: false });
    }, 1500);
  });

});


// browser-sync
gulp.task('browser-sync', ['nodemon'], function () {
  browserSync.init({
    files: ['public/**/*.*'],
    proxy: 'http://localhost:8080',
    port: 4000,
  });
});


gulp.task('default', ['styles','browser-sync'], function () {

  gulp.watch('public/sass/*.sass', ['styles']);
  gulp.watch(['public/css/*.css, public/js/**/*.js']).on('change', reload);

});

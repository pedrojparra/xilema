var Redcomercial = [
  {
    'zona':'Almería',
    'representante':'Antonio Quesada',
    'email':'aquesadaalvarez@gmail.com',
    'telefono':'629808870'
  },
  {
    'zona':'Aragón',
    'representante':'Juan A. Lacampa',
    'email':'ja.lacampahijar@telefonica.net',
    'telefono':'696958759'
  },
  {
    'zona':'Asturias',
    'representante':'Juan C. Llamedo',
    'email':'jcllamedo@hotmail.com',
    'telefono':'609159078'
  },
  {
    'zona':'Balerares',
    'representante':'Carlos Suasi',
    'email':'csuasi@gmail.com',
    'telefono':'649815065'
  },
  {
    'zona':'Cádiz',
    'representante':'Paco Lerma',
    'email':'pacoasislerma@gmail.com',
    'telefono':'606157850'
  },
  {
    'zona':'Castilla',
    'representante':'Juan J. Pavon',
    'email':'jjpavon@cgac.es',
    'telefono':'667550323'
  },
  {
    'zona':'Ciudad Real',
    'representante':'Jose A. Aguirre',
    'email':'joseantonioaguirremora@gmail.com',
    'telefono':'615174924'
  },
  {
    'zona':'Ciudad Real',
    'representante':'Angel Donate',
    'email':'angellopez08@hotmail.es',
    'telefono':'629224847'
  },
  {
    'zona':'Córdoba',
    'representante':'Jose A. Reina',
    'email':'jareinar@live.com',
    'telefono':'609574070'
  },
  {
    'zona':'Extremadura',
    'representante':'Manuel Herrera',
    'email':'manuelherreramartin@gmail.com',
    'telefono':'616495972'
  },
  {
    'zona':'Granada',
    'representante':'Antonio Quesada',
    'email':'aquesadaalvarez@gmail.com',
    'telefono':'629808870'
  },
  {
    'zona':'Huelva',
    'representante':'Javier Bonilla',
    'email':'rep.bonilla@telefonica.net',
    'telefono':'609150708'
  },
  {
    'zona':'Jaén',
    'representante':'Daniel Reina',
    'email':'danireina78@yahoo.es',
    'telefono':'609429657'
  },
  {
    'zona':'Málaga',
    'representante':'Jose M. Gris',
    'email':'representacionesgrisferrer@hotmail.com',
    'telefono':'605966014'
  },
  {
    'zona':'Málaga',
    'representante':'Javier Bleda',
    'email':'bledagarcia@gmail.com',
    'telefono':'610794539'
  },
  {
    'zona':'Murcia',
    'representante':'Antonio Ortega',
    'email':'aortegarepresentaciones@gmail.com',
    'telefono':'619054954'
  },
  {
    'zona':'Orense - Pontevedra',
    'representante':'Avelino',
    'email':'avelinomerellesc@hotmail.es',
    'telefono':'651038619'
  },
  {
    'zona':'Sevilla',
    'representante':'Javier Bonilla',
    'email':'rep.bonilla@telefonica.net',
    'telefono':'609150708'
  },
  {
    'zona':'Toledo',
    'representante':'Antonio Rubio',
    'email':'anrupe2203@gmail.com',
    'telefono':'610535095'
  },
  {
    'zona':'Valencia - Castellón',
    'representante':'Juan Comes',
    'email':'kubic_1@hotmail.com',
    'telefono':'639331986'
  },
  {
    'zona':'Valencia - Castellón',
    'representante':'Alberto Pérez',
    'email':'kubic_2@hotmail.com',
    'telefono':'605256080'
  }
];
module.exports = Redcomercial;

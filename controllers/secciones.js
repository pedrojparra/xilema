// Variables comunes
module.exports = [
  { title: 'Inicio', link: 'inicio', url: '/' },
  { title: 'Productos', link: 'productos', url: '/productos' },
  { title: 'Catálogos', link: 'catalogos', url: '/catalogos' },
  { title: 'Contacto', link: 'contacto', url: '/contacto' }
];

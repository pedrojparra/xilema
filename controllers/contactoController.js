// Nodemailer config
var nodemailer = require('nodemailer');
var validator = require('validator');
var smtpTransport = require('nodemailer-smtp-transport');

var transporter = nodemailer.createTransport(smtpTransport({
  host: 'smtp.1and1.es',
  port: 587,
  auth: {
    user: 'info@xilemamobiliario.com',
    pass: 'Luc.500-xilemob'
  }
}));


var seccionesMenu = require('./secciones');
var comericales = require('../data/redComercial');
exports.index = function(req, res){
  res.render('contacto', {

    seo: {
      title: 'Xilema. Contacto.',
      description: 'Fabricante de muebles de salón, dormitorios y auxiliares con gran variedad de acabados y diseños.',
      keywords: 'Muebles, mobiliario, xilema, dormitorios, salones, auxiliares, acabados, fénix, dianne, vip, lucena, silla, sillón, vitrina, cómoda, mesita, mesa, comodón, cabecero, armario, módulo, modular, espejo, vestidor, xinfonier, sinfonier, aparador, librero, estantería, espejo, marco, multifunción, vinoteca, tirador, tiradores, TV, expositor, extensible, guaserí, pintura, laca, barniz, lacado/s, barnizado/s, lija, comedores, diseño, furniture, bedroom, living, assistant, chair, armchair, alive, gruponf, orni'
    },
    secciones: seccionesMenu,
    linkActivo: 'contacto',
    comerciales: comericales
  });
};

// Ajax Form
exports.ajaxForm = function(req, res){

  var validNombre = false, validEmail = false, validPhone = false, validMessage = false;

  validEmail = validator.isEmail( req.body.email );
  validPhone = validator.isNumeric( validator.escape(req.body.phone) );
  validCiudad = req.body.ciudad || 'Ciudad no disponible.';
  if( req.body.nombre || req.body.nombre.length ){ validNombre = true; }
  if( req.body.message || req.body.nombre.length ){ validMessage = true; }

  if( validNombre === false || validEmail === false || validPhone === false || validMessage === false ){

    res.json({'status':'error', 'info':'validatorFail'});

  }else{

    // Send Maill

    var finalSms = 'Email recibido de: ' + validator.escape(req.body.nombre);
    finalSms += '<br/>Con telefono: ' +  req.body.phone;
    finalSms += '<br/>Con email: ' + req.body.email;
    finalSms += '<br/>Desde la ciudad de: ' + validCiudad;
    finalSms += '<br/>' + req.body.message;

    transporter.sendMail({
      from: 'info@xilemamobiliario.com',
      to: 'info@xilemamobiliario.com',
      subject: 'Mensaje de xilemamobiliario.com',
      html: finalSms
    }, function(error){
      if(error){
        console.log(error);
        res.json({'status':'error', 'info':'sendFail'});
      }else{
        res.json({'status':'enviado'});
      }
    });

  }

};

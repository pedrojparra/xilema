var seccionesMenu = require('./secciones');

exports.load = function (req, res, next, productoId) {
  req.productoId = productoId;
  next();
};

exports.index = function (req, res) {
  res.render('productos', {

    seo: {
      title: 'Xilema. Productos',
      description: 'Fabricante de muebles de salón, dormitorios y auxiliares con gran variedad de acabados y diseños.',
      keywords: 'Muebles, mobiliario, xilema, dormitorios, salones, auxiliares, acabados, fénix, dianne, vip, lucena, silla, sillón, vitrina, cómoda, mesita, mesa, comodón, cabecero, armario, módulo, modular, espejo, vestidor, xinfonier, sinfonier, aparador, librero, estantería, espejo, marco, multifunción, vinoteca, tirador, tiradores, TV, expositor, extensible, guaserí, pintura, laca, barniz, lacado/s, barnizado/s, lija, comedores, diseño, furniture, bedroom, living, assistant, chair, armchair, alive, gruponf, orni'
    },
    secciones: seccionesMenu,
    linkActivo: 'productos'

  });
};

exports.show = function (req, res) {
  res.render('producto-detail', {
    seo: {
      title: 'Xilema. Productos',
      description: 'Fabricante de muebles de salón, dormitorios y auxiliares con gran variedad de acabados y diseños.',
      keywords: 'Muebles, mobiliario, xilema, dormitorios, salones, auxiliares, acabados, fénix, dianne, vip, lucena, silla, sillón, vitrina, cómoda, mesita, mesa, comodón, cabecero, armario, módulo, modular, espejo, vestidor, xinfonier, sinfonier, aparador, librero, estantería, espejo, marco, multifunción, vinoteca, tirador, tiradores, TV, expositor, extensible, guaserí, pintura, laca, barniz, lacado/s, barnizado/s, lija, comedores, diseño, furniture, bedroom, living, assistant, chair, armchair, alive, gruponf, orni'
    },
    secciones: seccionesMenu,
    linkActivo: 'productos',
    idProducto: req.productoId

  });
};
